package com.example.m7m8_pr3country.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.m7m8_pr3country.R
import com.example.m7m8_pr3country.databinding.FragmentFavoritesBinding

class FavoritesFragment : Fragment() {
    lateinit var binding: FragmentFavoritesBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavoritesBinding.inflate(layoutInflater)
            return binding.root
    }


}