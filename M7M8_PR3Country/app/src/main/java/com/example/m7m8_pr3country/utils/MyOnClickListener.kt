package com.example.m7m8_pr3country.utils

import com.example.m7m8_pr3country.model.CountryItem

interface MyOnClickListener {
    fun onClick(country: CountryItem)
}