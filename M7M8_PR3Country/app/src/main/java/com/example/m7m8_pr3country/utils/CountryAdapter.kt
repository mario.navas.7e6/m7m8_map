package com.example.m7m8_pr3country.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.m7m8_pr3country.R
import com.example.m7m8_pr3country.model.CountryItem
import com.example.m7m8_pr3country.databinding.ItemCountryBinding
import com.example.m7m8_pr3country.view.RecyclerViewFragment


class CountryAdapter(private val countries: List<CountryItem>, private val listener: RecyclerViewFragment): RecyclerView.Adapter<CountryAdapter.ViewHolder>(){
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemCountryBinding.bind(view)
        fun setListener(country: CountryItem){
            binding.root.setOnClickListener {
                listener.onClick(country)
            }
        }
    }
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_country, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val country = countries[position]
        with(holder) {
            setListener(country)
            binding.itemCountryText.text = country.name
            binding.itemCountryId.text = country.id.toString()
            context?.let {
                Glide.with(it)
                    .load(country.media.flag)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .circleCrop()
                    .into(binding.itemCountryImage)
            }
        }


    }

    override fun getItemCount(): Int {
        return countries.size
    }





}