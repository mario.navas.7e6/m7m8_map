package com.example.m7m8_pr3country.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.m7m8_pr3country.R
import com.example.m7m8_pr3country.databinding.FragmentDetailBinding
import com.example.m7m8_pr3country.viewModel.CountryViewModel


class DetailFragment : Fragment() {
    lateinit var binding: FragmentDetailBinding
    private val viewModel: CountryViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(requireContext())
            .load(viewModel.currentCountry.media.orthographic)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .circleCrop()
            .into(binding.detailImage)
    }

}

