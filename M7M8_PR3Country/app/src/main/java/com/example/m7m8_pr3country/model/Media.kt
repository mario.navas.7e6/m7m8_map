package com.example.m7m8_pr3country.model

data class Media(
    val emblem: String,
    val flag: String,
    val orthographic: String
)