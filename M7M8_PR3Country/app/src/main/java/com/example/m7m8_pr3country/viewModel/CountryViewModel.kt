package com.example.m7m8_pr3country.viewModel

import android.provider.ContactsContract
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.m7m8_pr3country.model.Countries
import com.example.m7m8_pr3country.model.CountryItem
import com.example.m7m8_pr3country.model.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CountryViewModel : ViewModel() {
    private val repository = Repository()
    var data = MutableLiveData<Countries?>()
    lateinit var currentCountry: CountryItem
    init {
        fetchData()
    }

    private fun fetchData() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getCountries()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    data.postValue(response.body())
                } else {
                    Log.e("Error :", response.message())
                }
            }
        }
    }

    fun setSelectedCountry(country: CountryItem){
        currentCountry = country
    }

    fun searchCountry(name:String):List<CountryItem>{
        val countries = mutableListOf<CountryItem>()
        for(i in data.value!!.indices){
            if(name in data.value!![i].name) countries.add(data.value!![i])
        }
        return countries
    }

}
