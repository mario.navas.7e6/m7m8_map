package com.example.m7m8_map.view

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.m7m8_map.R
import com.example.m7m8_map.model.Marker
import com.example.m7m8_map.viewModel.MarkerViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

const val REQUEST_CODE_LOCATION = 100

class MapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMapLongClickListener {
    private val db = FirebaseFirestore.getInstance()
    private val auth = FirebaseAuth.getInstance()
    lateinit var map: GoogleMap
    private val viewModel: MarkerViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView =  inflater.inflate(R.layout.fragment_map, container, false)
        createMap()
        return rootView
    }

    fun createMap(){
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as     SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.setOnMapLongClickListener (this)
        createMarker()
        enableLocation()
    }

    fun cameraZoom(marker: Marker){
        val coordinates = LatLng(marker.latitude, marker.longitude)
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(coordinates, 10f), 4000, null)
    }


    fun createMarker(){
//        for(i in viewModel.getMarkers()){
//            val coordinates = LatLng(i.latitude, i.longitude)
//            val myMarker = MarkerOptions().position(coordinates).title(i.name)
//            map.addMarker(myMarker)
//            map.animateCamera(
//                CameraUpdateFactory.newLatLngZoom(coordinates, 10f),
//                5000, null)
//        }
        db.collection("users").document(auth.currentUser?.email!!).collection("markers").get().addOnSuccessListener {
            for (document in it) {
                val coordinates= LatLng(document["latitude"] as Double,document["longitude"] as Double)
                val myMarker = MarkerOptions().position(coordinates).title(document["name"] as String?)
                map.addMarker(myMarker)
            }
        }

    }

    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(),
            ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }


    @SuppressLint("MissingPermission")
    private fun enableLocation(){
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted()){
            map.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }

    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                ACCESS_FINE_LOCATION)){
            Toast.makeText(requireContext(), "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION
            )
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when(requestCode){
            REQUEST_CODE_LOCATION -> if(grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED){
                map.isMyLocationEnabled = true
            }
            else{
                Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if(!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            map.isMyLocationEnabled = false
            Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                Toast.LENGTH_SHORT).show()
        }
    }



    override fun onMapLongClick(p0: LatLng) {
        val latitude = p0.latitude
        val longitude = p0.longitude
        val createMarker =  MapFragmentDirections.actionMapFragmentToCreateFragment(
                                latitude.toFloat(),
                                longitude.toFloat()
                            )
        viewModel.latitude= latitude
        viewModel.longitude = longitude
        findNavController().navigate(createMarker)

    }


}