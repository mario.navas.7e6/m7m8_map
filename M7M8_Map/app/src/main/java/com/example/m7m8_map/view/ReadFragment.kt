package com.example.m7m8_map.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.m7m8_map.R
import com.example.m7m8_map.databinding.FragmentReadBinding
import com.example.m7m8_map.viewModel.MarkerViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore


class ReadFragment : Fragment() {
    private val db = FirebaseFirestore.getInstance()
    private val auth = FirebaseAuth.getInstance()
    lateinit var binding: FragmentReadBinding
    private val viewModel: MarkerViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentReadBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var marker = viewModel.selectedMarker
        binding.inputName.text = marker.name
//        binding.inputPicture
        binding.description.text = marker.desc

        binding.deleteFragment.setOnClickListener{
            db.collection("users").document(auth.currentUser?.email!!).collection("markers").document(binding.inputName.text.toString()).delete()
        }

        binding.updateFragment.setOnClickListener{
            findNavController().navigate(R.id.action_readFragment_to_updateFragment)
        }
    }


}