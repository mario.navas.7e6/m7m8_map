package com.example.m7m8_map.viewModel

import android.net.Uri
import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.m7m8_map.model.Marker
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.QuerySnapshot
import java.util.*

class MarkerViewModel : ViewModel(

)
{
    private val db = FirebaseFirestore.getInstance()
    private val markers = mutableListOf<Marker>()
    var latitude : Double = 0.0
    var longitude: Double = 0.0
    var image: Uri? = null
    var name: String = ""
    lateinit var selectedMarker: Marker
    lateinit var email: String
    lateinit var password: String

    fun addMarker(marker: Marker) {
        markers.add(marker)
    }

    fun removeMarker(marker: Marker) {
        markers.remove(marker)
    }

    fun getMarkers(): List<Marker> {
        return markers
    }


//    fun eventChangeListener() {
//        db.collection("markers").addSnapshotListener(object: EventListener<QuerySnapshot>,
//            com.google.firebase.firestore.EventListener<QuerySnapshot> {
//            override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
//                if(error != null){
//                    Log.e("Firestore error", error.message.toString())
//                    return
//                }
//                for(dc: DocumentChange in value?.documentChanges!!){
//                    if(dc.type == DocumentChange.Type.ADDED){
//                        val newMarker = dc.document.toObject(Marker::class.java)
//                        email = dc.document.id
//                        addMarker(newMarker)
//                    }
//                }
//            }
//        })
//    }

}

