
package com.example.m7m8_map.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.edit
import androidx.core.text.set
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.m7m8_map.R
import com.example.m7m8_map.databinding.FragmentLoginBinding
import com.example.m7m8_map.viewModel.MarkerViewModel
import com.google.firebase.auth.FirebaseAuth

class LoginFragment : Fragment() {
    lateinit var myPreferences: SharedPreferences

    lateinit var binding: FragmentLoginBinding
    private val viewModel: MarkerViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        if(myPreferences.getBoolean("active", false)){
//            findNavController().navigate(R.id.action_loginFragment_to_mapFragment)
//        }
        myPreferences = requireActivity().getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE)
        if(myPreferences.getBoolean("remember",false)){
            binding.userEmail.setText(myPreferences.getString("email", ""))
            binding.password.setText(myPreferences.getString("password", ""))
            binding.rememberCheckbox.isChecked = true
        }
        binding.login.setOnClickListener{
            FirebaseAuth.getInstance().
            signInWithEmailAndPassword(binding.userEmail.text.toString(), binding.password.text.toString())
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        if(binding.rememberCheckbox.isChecked){
                            val emailLogged = it.result?.user?.email
                            viewModel.email = emailLogged!!
                            myPreferences.edit{
                                putString("email", binding.userEmail.text.toString())
                                putString("password", binding.password.text.toString())
                                putBoolean("remember", true)
                                putBoolean("active", true)
                                apply()
                            }

                        }else{
                            val emailLogged = it.result?.user?.email
                            viewModel.email = emailLogged!!
                            myPreferences.edit{
                                putString("email", binding.userEmail.text.toString())
                                putString("password", binding.password.text.toString())
                                putBoolean("remember", false)
                                putBoolean("active", true)
                                apply()
                            }
                        }

                        findNavController().navigate(R.id.action_loginFragment_to_mapFragment)
                    //                        goToHome(emailLogged!!)
                    }
                    else{
                        val toast = Toast.makeText(context,"Error Singing", Toast.LENGTH_SHORT)
                        toast.show()
                    }
                }
        }
        binding.createAccount.setOnClickListener{
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }

    }
}

