package com.example.m7m8_map.view

import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.edit
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.m7m8_map.R
import com.example.m7m8_map.databinding.FragmentRegisterBinding
import com.example.m7m8_map.viewModel.MarkerViewModel
import com.google.firebase.auth.FirebaseAuth


class RegisterFragment : Fragment() {
    lateinit var myPreferences: SharedPreferences

    lateinit var binding: FragmentRegisterBinding
    private val viewModel: MarkerViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegisterBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.register.setOnClickListener {
            FirebaseAuth.getInstance().
            createUserWithEmailAndPassword(binding.userEmail.text.toString(), binding.password.text.toString())
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        if(binding.rememberCheckbox.isChecked){
                            val emailLogged = it.result?.user?.email
                            viewModel.email = emailLogged!!
                            myPreferences.edit{
                                putString("email", binding.userEmail.text.toString())
                                putString("password", binding.password.text.toString())
                                putBoolean("remember", true)
                                putBoolean("active", true)
                                apply()
                            }

                        }else{
                            val emailLogged = it.result?.user?.email
                            viewModel.email = emailLogged!!
                            myPreferences.edit{
                                putString("email", binding.userEmail.text.toString())
                                putString("password", binding.password.text.toString())
                                putBoolean("remember", false)
                                putBoolean("active", true)
                                apply()
                            }
                        }
                        val emailLogged = it.result?.user?.email
                        viewModel.email = emailLogged!!
                        findNavController().navigate(R.id.action_registerFragment_to_mapFragment3)
                      }
                    else{
                      val toast = Toast.makeText(context,"Error in Registration", Toast.LENGTH_SHORT)
                        toast.show()
                    }
                }

        }
    }
}