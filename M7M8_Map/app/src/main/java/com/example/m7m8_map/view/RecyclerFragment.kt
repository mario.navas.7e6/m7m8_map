package com.example.m7m8_map.view

import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.m7m8_map.R
import com.example.m7m8_map.databinding.FragmentRecyclerBinding
import com.example.m7m8_map.model.Marker
import com.example.m7m8_map.utils.MarkerAdapter
import com.example.m7m8_map.utils.MyOnClickListener
import com.example.m7m8_map.viewModel.MarkerViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.google.firebase.firestore.ktx.toObject

class RecyclerFragment : Fragment(), MyOnClickListener {
    private val db = FirebaseFirestore.getInstance()
    lateinit var markersList : ArrayList<Marker>
    private val auth = FirebaseAuth.getInstance()
    private lateinit var markerAdapter: MarkerAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentRecyclerBinding
    private val viewModel : MarkerViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRecyclerBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        setUpRecyclerView(viewModel.getMarkers())
        setUpRecyclerView(arrayListOf())
        markersList = arrayListOf<Marker>()
        eventChangeListener()


    }

    override fun map(marker: Marker) {
        viewModel.selectedMarker = marker
        findNavController().navigate(R.id.mapFragment)
    }
    override fun detail(marker: Marker) {
        viewModel.selectedMarker = marker
        findNavController().navigate(R.id.readFragment)
    }

    private fun setUpRecyclerView(listOfMarkers: List<Marker>){
        markerAdapter = MarkerAdapter(listOfMarkers, this)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.apply{
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = markerAdapter
        }
    }


    private fun eventChangeListener() {
        db.collection("users").document(auth.currentUser?.email!!).collection("markers").addSnapshotListener(object: EventListener<QuerySnapshot> {
            override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                if(error != null){
                    Log.e("Firestore error", error.message.toString())
                    return
                }
                for(dc: DocumentChange in value?.documentChanges!!){
                    if(dc.type == DocumentChange.Type.ADDED){
                        var values = dc.document.getData()
                        val newLatitude = dc.document
                        val newMarker = Marker(values["latitude"] as Double,
                                                values["longitude"] as Double,
                                                values["name"] as String,
                                                values["image"] as String,
                                                values["desc"] as String
                        )
//                        val newMarker = dc.document.toObject(Marker::class.java)
//                        newMarker.email = dc.document.id
                        markersList.add(newMarker)
                    }
                }
                markerAdapter.setMarkerList(markersList)
            }
        })
    }






}