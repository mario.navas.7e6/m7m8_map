package com.example.fragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    supportFragmentManager.beginTransaction().apply {
        replace(R.id.fragmentContainerView, FragmentTest())
        setReorderingAllowed(true)
        addToBackStack("name") // name can be null
        commit()
    }

}