package com.example.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.fragments.databinding.FragmentTestBinding


class FragmentTest : Fragment() {

    lateinit var binding: FragmentTestBinding

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentTestBinding.inflate(layoutInflater)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonNextFragment.setOnClicklistener

        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, FragmentTest())
            setReorderingAllowed(true)
            addToBackStack("name") // name can be null
            commit()
        }
    }


}