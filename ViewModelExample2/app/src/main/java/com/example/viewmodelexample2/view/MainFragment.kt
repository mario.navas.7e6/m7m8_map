package com.example.viewmodelexample2.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.viewmodelexample2.databinding.FragmentMainBinding
import com.example.viewmodelexample2.viewmodel

class MainFragment : Fragment() {

    lateinit var binding: FragmentMainBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentMainBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val userViewModel = ViewModelProvider(requireActivity()).get(UsersViewModel::class.java)
        userViewModel.user.observe(viewLifecycleOwner, {
            binding.userNameTv.text = it.userName
            binding.userAgeTv.text = it.userAge.toString()
        })

        binding.randomButton.setOnClickListener {
            userViewModel.getRandomUser()
        }
    }
}