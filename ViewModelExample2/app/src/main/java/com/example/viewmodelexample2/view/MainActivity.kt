package com.example.viewmodelexample2.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.viewmodelexample2.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, MainFragment())
            setReorderingAllowed(true)
            addToBackStack("name") // name can be null
            commit()
        }

    }
}