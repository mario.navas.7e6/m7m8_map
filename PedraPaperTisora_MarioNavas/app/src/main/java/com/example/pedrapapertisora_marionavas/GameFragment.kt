import com.example.pedrapapertisora_marionavas.R

hpackage com.example.pedrapapertisora_marionavas

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import com.example.pedrapapertisora_marionavas.databinding.FragmentGameBinding
import com.example.pedrapapertisora_marionavas.databinding.FragmentMenuBinding


class GameFragment : Fragment() {
    lateinit var binding: FragmentGameBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGameBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val tijeraComparar = ResourcesCompat.getDrawable(getResources(), R.drawable.tijera, null)!!.constantState
        val piedraComparar = ResourcesCompat.getDrawable(getResources(), R.drawable.piedra, null)!!.constantState
        val papelComparar = ResourcesCompat.getDrawable(getResources(), R.drawable.papel, null)!!.constantState


        val imageCPUComparar = binding.cpuImage.drawable.constantState



        val timer = object : CountDownTimer(3000, 100) {
            override fun onTick(millisUntilFinished: Long) {



                if(millisUntilFinished<1000){
                    binding.cpuCounter.text = "1"

                }
                else if(millisUntilFinished<2000){
                    binding.cpuCounter.text = "2"
                }

            }
            override fun onFinish() {
                binding.cpuImage.visibility = VISIBLE
                binding.cpuCounter.visibility = INVISIBLE
                compare()
                binding.nextRound.visibility = VISIBLE
            }
        }.start()

        binding.playerImage.setOnClickListener{
            when (binding.playerImage.drawable.constantState) {
                piedraComparar -> {
                    binding.playerImage.setImageResource(R.drawable.papel)
                }
                papelComparar -> {
                    binding.playerImage.setImageResource(R.drawable.tijera)
                }
                else -> {
                    binding.playerImage.setImageResource(R.drawable.piedra)
                }
            }
        }

    }
    @SuppressLint("SetTextI18n")
    fun compare(){
        var cpuWin = 0
        var playerWin = 0
        val tijeraComparar = ResourcesCompat.getDrawable(getResources(), R.drawable.tijera, null)!!.constantState
        val piedraComparar = ResourcesCompat.getDrawable(getResources(), R.drawable.piedra, null)!!.constantState
        val papelComparar = ResourcesCompat.getDrawable(getResources(), R.drawable.papel, null)!!.constantState

        val imageUserComparar = binding.playerImage.drawable.constantState
        val imageCPUComparar = binding.cpuImage.drawable.constantState

        if(imageUserComparar!=imageCPUComparar){
            if(imageUserComparar==tijeraComparar&&imageCPUComparar==papelComparar){
                playerWin++
            }
            else if(imageUserComparar==tijeraComparar&&imageCPUComparar==piedraComparar){
                cpuWin++
            }
            if(imageUserComparar==piedraComparar&&imageCPUComparar==papelComparar){
                cpuWin++
            }
            else if(imageUserComparar==piedraComparar&&imageCPUComparar==tijeraComparar){
               playerWin
            }
            if(imageUserComparar==papelComparar&&imageCPUComparar==piedraComparar){
                playerWin++
            }
            else if(imageUserComparar==papelComparar&&imageCPUComparar==tijeraComparar){
                cpuWin++
            }
        }
        binding.cpuCounter.text = "${playerWin} - ${cpuWin}"

    }


}